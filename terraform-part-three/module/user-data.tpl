runcmd:
  - "sudo yum install -y httpd"
  - "sudo systemctl start httpd"
  - "sudo yum install -y nginx"
  - "aws s3 cp s3://schl-case-study-zain/default.conf ./"
  - "sudo rm /etc/nginx/conf.d/default.conf"
  - "sudo cp default.conf /etc/nginx/conf.d/"
  - "sudo systemctl start nginx"
