variable "candidate"{
  type = string
  default = "zain-part-three"
}

variable "tags"{
  type = map
  default = {
    Name = "case-study-test"
  }

}


variable "vpc_security_group_ids" {
  type = list 
  default = ["sg-02073367768e306bb"] 
}
